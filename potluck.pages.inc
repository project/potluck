<?php

//////////////////////////////////////////////////////////////////////////////
// Menu callbacks

function potluck_output_mix() {
  $form = array();

  $form['info'] = array('#value' => '<p style="font-size: 150%;">' . 'Paste the addresses of <a href="http://simile.mit.edu/exhibit/">Exhibit</a>-powered pages containing data to mix, one address per line. Then click Mix Data.</p>');
  $form['urls'] = array('#type' => 'textarea', '#title' => '', '#rows' => 10, '#style' => 'width: 100%;');
  $form['submit'] = array('#type' => 'submit', '#value' => t('Mix Data'), '#xsubmit' => array('potluck_output_mix_submit'), '#id' => 'mix-button', '#attributes' => array('style' => 'width: 10em; height: 2.5em; font-size: 100%;'));

  return $form;
}

function potluck_output_mix_validate($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  $urls = array_filter(explode("\n", str_replace(array("\n\n", "\r"), array("\n", ''), $urls)), 'strlen');
  if (empty($urls)) {
    form_set_error('urls', t('No URLs specified.'));
  }
  else {
    foreach ($urls as $url) {
      if (!valid_url($url, TRUE)) {
        form_set_error('urls', t('%url is not a valid URL.', array('%url' => $url)));
      }
    }
  }
}

function potluck_output_mix_submit($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  file_check_directory($path = POTLUCK_CACHE_DIR, TRUE);

  $id = sha1(implode("\n", $urls));
  file_put_contents(potluck_get_path($id), drupal_to_js(potluck_mix($urls)));
  $_SESSION['potluck'] = $id;

  $form_state['redirect'] = array('potluck/view', array('id' => $id));
}

function potluck_output_view() {
  if (!isset($_SESSION['potluck'])) {
    return drupal_goto('potluck');
  }

  drupal_add_css(drupal_get_path('module', 'potluck') .'/potluck.css');

  // Load the SIMILE Exhibit & Potluck APIs
  drupal_set_html_head('<script type="text/javascript" src="'. EXHIBIT_JS_URL .'"></script>'."\n");
  drupal_set_html_head('<script type="text/javascript" src="'. POTLUCK_JS_URL .'"></script>'."\n");

  $code = array(
    'SimileAjax.History.historyFile = "'. url('potluck/__history__.html') .'";',
    '$(document).ready(function() {',
    '  setTimeout(function () { Potluck.create(document.getElementById("header-pane"), document.getElementById("content-pane")); }, 500);',
    '});',
  );
  drupal_add_js(implode("\n", $code), 'inline');

  //exhibit_add_feed(url('potluck/data'));
  exhibit_add_feed(url(potluck_get_path($_SESSION['potluck'])));

  drupal_set_header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
  drupal_set_header('Expires: -1');
  drupal_set_header('Cache-Control: no-cache, must-revalidate');
  drupal_set_header('Pragma: no-cache');

  return theme('potluck_page');
}

function potluck_output_data() {
  require_once drupal_get_path('module', 'exhibit') .'/exhibit.pages.inc';

  if (isset($_SESSION['potluck'])) {
    $filepath = potluck_get_path($_SESSION['potluck']);
    if (file_exists($filepath)) {
      drupal_set_header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
      drupal_set_header('Expires: -1');
      drupal_set_header('Cache-Control: no-cache, must-revalidate');
      drupal_set_header('Pragma: no-cache');

      return exhibit_output('application/json', file_get_contents($filepath));
    }
  }
  return drupal_not_found();
}

/**
 * @see http://simile.mit.edu/wiki/Exhibit/Template/_history_.html
 */
function potluck_output_history() {
  print '<html><body></body></html>';
}
